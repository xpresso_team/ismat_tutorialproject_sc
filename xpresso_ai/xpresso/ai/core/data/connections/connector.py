""" Factory class for Data-Connectivity  """

__author__ = 'Shlok Chaudhari'
__all__ = 'connector'


import enum
from xpresso.ai.core.data.connections.factory_fs import FSConnectorFactory
from xpresso.ai.core.data.connections.factory_db import DBConnectorFactory


class Datasources(enum.Enum):
    """

    Enum class that lists datasources supported by
    Xpresso Data-Connection library

    """

    DB = "DB"
    FS = "FS"


class Connector:
    """

    Factory class to provide Connector object of specified type

    """

    @staticmethod
    def getconnector(user_datasource, datasource_type=None):
        """

        This method returns Connector object of a specific datasource

        :param datasource_type: attribute to specify "Local" or "BigQuery"
                to instantiate local filesystem or GCP BigQuery. Default is
                None, which invokes a distributed filesystem or database
                management module called Alluxio or Presto, respectively.
        :param user_datasource: a string object stating the
                                datasource ("FS" or "DB")

        :return: Connector object

        """

        if user_datasource == Datasources.FS.value:
            return FSConnectorFactory().getconnector(datasource_type)
        elif user_datasource == Datasources.DB.value:
            return DBConnectorFactory().getconnector(datasource_type)
